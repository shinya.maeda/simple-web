## Description

This is the simple web app.

## How To configure

```shell
devkitkat configure all
devkitkat start postgres
devkitkat seed rails
```

## How To start

```shell
devkitkat start all
```
